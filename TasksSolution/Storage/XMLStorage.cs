﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Tasks.DataLayerAbstract;
using Tasks.ModelAbstract;

namespace Tasks.Storage
{
    public class XMLStorage<TEntity, TId>: 
        SerializedStore<TEntity, TId> where TEntity : Entity<TId>, new()
    {
        public XMLStorage(string filePath):base(filePath) { }

        public override IEnumerable<TEntity> Deserialize(string content)
        {
            IEnumerable<TEntity> result;
            var serializer = new XmlSerializer(typeof(IEnumerable<TEntity>));
            using(TextReader reader = new StringReader(content))
                result = (IEnumerable<TEntity>)serializer.Deserialize(reader);
            return result;
        }

        public override string Serialize(IEnumerable<TEntity> objectsToSerialize)
        {
            XmlSerializer xmlFormat = new XmlSerializer(typeof(IEnumerable<TEntity>));
            string result;
            using (StringWriter textWriter = new StringWriter())
            {
                xmlFormat.Serialize(textWriter, objectsToSerialize);
                result = textWriter.ToString();
            }

            return result;
        }
    }
}

