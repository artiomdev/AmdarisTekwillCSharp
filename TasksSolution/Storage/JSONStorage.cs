﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Tasks.DataLayerAbstract;
using Tasks.ModelAbstract;

namespace Tasks.Storage
{
    /// <summary>
    /// Сериализация\десериализация объектов JSON
    /// </summary>
    public class JSONStore<TEntity, TId>: 
        SerializedStore<TEntity, TId> where TEntity: Entity<TId>
    {
        public JSONStore(string JSONFilePath): base(JSONFilePath) { }

        public override IEnumerable<TEntity> Deserialize(string content) =>
            JsonConvert.DeserializeObject<IEnumerable<TEntity>>(content);

        public override string Serialize(IEnumerable<TEntity> entities) =>
            JsonConvert.SerializeObject(entities);
    }
}
