﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tasks.DataLayerAbstract;
using Tasks.Model;

namespace Tasks.Storage
{
    public class UserStorage: IStore<User, int>
    {
        public UserStorage()
        {
            Users = new List<User>()
            {
                new User("admin", "admin") {Id = 1},
                new User("Artiom", "pass") {Id = 2},
                new User("Alex", "pass") {Id = 3}
            };
        }

        public IEnumerable<User> Users { get; set; }
        public User GetEntity(int id)
        {
            return Users.Where(i => i.Id == id).SingleOrDefault();
        }

        public IEnumerable<User> GetAll()
        {
            return Users;
        }

        public IEnumerable<User> Entities { get; set; }
        public void CreateOrUpdate(User entity)
        {
            var existingUser = GetEntity(entity.Id);
            if(existingUser != null)
                existingUser.CopyFieldsFrom(entity);
            else
                Users.ToList().Add(entity);
        }

        public void Delete(User entity)
        {
            if(GetEntity(entity.Id) != null)
                Users.ToList().Remove(entity);
            throw new NoNullAllowedException("Нельзя удалить несуществующего пользователя.");
        }

        IList<User> IStore<User, int>.Entities
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
