﻿using System;
using System.Collections.Generic;

namespace Tasks.ModelAbstract
{
    public abstract class Entity<TId>: IEntity<TId>
    {
        public abstract TId Id { get; set; }

        /// <summary>
        /// "Безопасное" редактирование сущности посредством
        /// копироапния необходимых полей редактируемого объекта для последующего сохранения
        /// </summary>
        /// <param name="entity">Редактируемый объект</param>
        public abstract void CopyFieldsFrom(IEntity<TId> entity);

        /// <summary>
        /// Определяет и устанавливает сущности уникальный идентификатор
        /// </summary>
        /// <param name="entities">Коллекция существующих сущьностей</param>
        public abstract void CreateNextUniqueId(IEnumerable<IEntity<TId>> entities);

    }
}
