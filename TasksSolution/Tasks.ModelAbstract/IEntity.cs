﻿namespace Tasks.ModelAbstract
{
    public interface IEntity<TId>
    {
        TId Id { get; }
    }
}
