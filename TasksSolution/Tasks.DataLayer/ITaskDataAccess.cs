﻿using System.Collections.Generic;
using Tasks.Model;

namespace Tasks.DataLayer
{
    public interface ITaskDataAccess
    {
        IEnumerable<Task> GetTasks();
        IEnumerable<Task> GetTasks(int pageNo, int pageSize);
        Task GetTask(Task task);
        Task GetTask(int taskId);
        IEnumerable<Task> GetTasksToExecute(User user);
        IEnumerable<Task> GetTasksByAuthor(User user);
        void SaveTask(Task task);
    }
}