﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tasks.Model;
using Tasks.Storage;
using Tasks.DataLayerAbstract;
using System.Xml.Linq;

namespace Tasks.DataLayer
{
    public class TaskDataAccess: DataAccess<Task, int>, ITaskDataAccess
    {
        public TaskDataAccess(IStore<Task, int> taskStorage): base(taskStorage)
        {
        }

        public IEnumerable<Task> GetTasks() => _store.Entities;

        public IEnumerable<Task> GetTasks(int pageNo, int pageSize)
        {
            var count = _store.Entities.Count();
            var maxAvailablePageNo = Math.Ceiling(Decimal.Divide(count, pageSize));
            if (maxAvailablePageNo < pageNo)
                throw new IndexOutOfRangeException("Requested page does not exist.");

            return _store.Entities.Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
        } 

        public Task GetTask(Task task)
        {
            if (task == null)
                throw new ArgumentNullException($"{nameof(Task)} can't be null.");

            return GetTask(task.Id);
        }

        public Task GetTask(int taskId)
        {
            return GetTasks()
                .SingleOrDefault(i => i.Id == taskId);
        }

        public IEnumerable<Task> GetTasksToExecute(User user)
        {
            return GetTasks().Where(i => i.ExecutorId == user.Id);
        }

        public IEnumerable<Task> GetTasksByAuthor(User user)
        {
            return GetTasks().Where(i => i.AuthorId == user.Id);
        }

        private void SaveTasks(IList<Task> tasks)
        {
            if (tasks == null || !tasks.Any())
                throw new ArgumentNullException($"{nameof(Task)} list can't be null or empty.");
            _store.Entities = tasks;
        }

        public void SaveTask(Task task)
        {
            _store.CreateOrUpdate(task);
        }

    }

}