﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tasks.Model;
using Tasks.DataLayerAbstract;

namespace Tasks.DataLayer
{
    public class UserDataAccess: DataAccess<User, int>, IUserDataAccess
    {
        private IEnumerable<User> Users { get; set; }

        public UserDataAccess(IStore<User, int> userStore):base(userStore)
        {
            Users = _store.GetAll();
        }

        public User GetUser(User user) => Users.SingleOrDefault(i => i.Id == user.Id);

        public User GetUser(string name, string password) =>
            Users.Where(i => i.Name == name).SingleOrDefault(i => i.Password == password);
        


    }
}
