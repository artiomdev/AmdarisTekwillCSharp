﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tasks.Model;

namespace Tasks.DataLayerAbstract
{
    public interface IUserDataAccess
    {
        User GetUser(User user);
        User GetUser(string name, string password);
    }
}
