﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tasks.ModelAbstract;

namespace Tasks.DataLayerAbstract
{
    public abstract class DataAccess<TEntity, TId>: IDataAccess<TEntity, TId> where TEntity : IEntity<TId>
    {
        protected IStore<TEntity, TId> _store;

        protected DataAccess(IStore<TEntity, TId> store)
        {
            _store = store;
        }

        public void CreateOrUpdate(TEntity entity)
        {
            _store.CreateOrUpdate(entity);
        }

        public TEntity GetById(TId id)
        {
            return _store.GetEntity(id);
        }

        public void Delete(TEntity entity)
        {
            _store.Delete(entity);
        }

        public IEnumerable<TEntity> Filter(Func<TEntity, bool> filter)
        {
            return _store.GetAll().Where(filter);
        }
    }
}
