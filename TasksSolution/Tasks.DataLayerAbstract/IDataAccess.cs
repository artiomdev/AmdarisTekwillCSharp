﻿using System;
using System.Collections.Generic;
using Tasks.ModelAbstract;

namespace Tasks.DataLayerAbstract
{
    public interface IDataAccess<TEntity, TId> where TEntity: IEntity<TId>
    {
        void CreateOrUpdate(TEntity entity);

        TEntity GetById(TId id);

        void Delete(TEntity entity);

        IEnumerable<TEntity> Filter(Func<TEntity, bool> filter);
    }
}