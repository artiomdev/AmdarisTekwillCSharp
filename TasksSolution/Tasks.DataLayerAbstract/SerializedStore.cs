﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasks.ModelAbstract;

namespace Tasks.DataLayerAbstract
{
    public abstract class SerializedStore<TEntity, TId> : ISerializedStore<TEntity, TId>,
        IStore<TEntity, TId> where TEntity: Entity<TId>
    {
        private string _filePath;

        /// <summary>
        /// Поле - "буфер", предназначенное для хранения сущностей во время операций с коллекцией
        /// Во избезание чтения - записи в файл на каждую операцию,
        /// все операции проводятся через это поле и записываются в файл лишь при вызове метода Save().
        /// Внимание: не обращаться к этому полю напрямую для чтения дянных или записи данных в методах, кроме Save()
        /// </summary>
        private IList<TEntity> _entities = new List<TEntity>();


        public SerializedStore(string filePath)
        {
            _filePath = filePath;
        }


        #region ISerializedStore implementation

        public IList<TEntity> Entities
        {
            get
            {
                if (_entities == null)
                    throw new NullReferenceException($"Something goes wrong: '{nameof(_entities)}' field is null");

                if (_entities.Any()) return _entities;

                if (File.Exists(_filePath))
                {
                    string content = File.ReadAllText(_filePath);
                    _entities = Deserialize(content).ToList();
                }

                return _entities;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException($"Can't set null value to '{nameof(_entities)}' field.");

                if (value.Any())
                    _entities = value;
            }
        }

        public void Save()
        {
            if (_entities == null)
                throw new ArgumentNullException($"Can't write null value from '{nameof(_entities)}' field to storage.");

            if (_entities.Any())
            {
                File.WriteAllText(
                    contents: Serialize(_entities),
                    path: _filePath);
            }

            // Очищаем буфер
            _entities = new List<TEntity>();
        }

        public abstract IEnumerable<TEntity> Deserialize(string content);

        public abstract string Serialize(IEnumerable<TEntity> entities);

        #endregion


        #region IStore implementation

        public IEnumerable<TEntity> GetAll() => Entities;

        public TEntity GetEntity(TId Id) => Entities.SingleOrDefault(i => i.Id.Equals(Id));

        public void CreateOrUpdate(TEntity entity)
        {
            TEntity existingEntity = GetEntity(entity.Id);

            if (existingEntity != null)
            {
                existingEntity.CopyFieldsFrom(entity);
            }
            else
            {
                entity.CreateNextUniqueId(Entities);
                Entities.Add(entity);
            }

            Save();
        }

        public void Delete(TEntity entity)
        {
            if (!Entities.Contains(entity))
                throw new AccessViolationException($"Couldn't remove {nameof(entity)} which does not exist.");

            bool isDeleted = Entities.Remove(entity);

            if (!isDeleted)
                throw new ApplicationException($"Unable to remove {nameof(entity)}");

        }

        #endregion
    }
}
