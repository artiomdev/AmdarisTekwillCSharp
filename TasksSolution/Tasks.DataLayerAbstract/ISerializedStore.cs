﻿using System;
using System.Collections.Generic;
using Tasks.DataLayerAbstract;
using Tasks.ModelAbstract;

namespace Tasks.DataLayerAbstract
{
    /// <summary>
    /// Запись\чтение из файла
    /// </summary>
    public interface ISerializedStore<TEntity, TId> where TEntity : IEntity<TId>
    {
        IEnumerable<TEntity> Deserialize(string content);
        string Serialize(IEnumerable<TEntity> objectsToSerialize);
    }
}
