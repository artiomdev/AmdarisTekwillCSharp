﻿using System;
using System.Collections.Generic;
using Tasks.ModelAbstract;

namespace Tasks.DataLayerAbstract
{
    public interface IStore<TEntity, TId> where TEntity : IEntity<TId>
    {
        IList<TEntity> Entities { get; set; }

        IEnumerable<TEntity> GetAll();

        TEntity GetEntity(TId id);

        void CreateOrUpdate(TEntity entity);

        void Delete(TEntity entity);

        /// <summary>
        /// Сохранение в хранилище результатов операций с сущьностями
        /// </summary>
        void Save();
    }
}
