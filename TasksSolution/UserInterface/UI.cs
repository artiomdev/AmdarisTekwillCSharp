﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tasks.DataLayer;
using Tasks.Model;
using Tasks.ModelAbstract;
using Tasks.SecurityAbstract;

namespace Tasks.UserInterface
{
    public class UI
    {
        private static ITaskDataAccess _taskDataAccess;
        private static ISecurity _security;

        public UI(ITaskDataAccess taskDataAccess, ISecurity security)
        {
            _taskDataAccess = taskDataAccess;
            _security = security;
        }

        /// <summary>
        /// Выводит на экран приветствие
        /// </summary>
        public void StartMsg()
        {
            Console.WriteLine("Welcome to Task Project!");
            Console.WriteLine("To start you should authorize.");
        }

        /// <summary>
        /// Выводит на экран задачи пользователя
        /// Projection
        /// </summary>
        private void ShowTasks(User user)
        {
            IEnumerable<Task> taskList = new List<Task>();

            Console.Clear();
            Console.WriteLine("Which tasks would you like to show?");
            Console.WriteLine("1. Tasks to execute");
            Console.WriteLine("2. Tasks you created");
            Console.WriteLine("3. All Tasks");
            Console.WriteLine("0. Exit");
            Console.WriteLine("Enter any other key to return back to main menu.");
            string action = Console.ReadLine();

            switch (action)
            {
                case "0":
                    return;
                    break;
                case "1":
                    taskList = _taskDataAccess.GetTasksToExecute(user);
                    break;
                case "2":
                    taskList = _taskDataAccess.GetTasksByAuthor(user);
                    break;
                case "3":
                    // TODO: return taskList
                    ShowAllTasks();
                    break;
                default:
                    Console.WriteLine($"Unknown action: '{action}'");
                    Console.WriteLine("Press any key and choose existing one.");
                    Console.ReadKey();
                    break;
            }

            if (!taskList.Any())
            {
                Console.Clear();
                Console.WriteLine("You haven't tasks.");
                Console.WriteLine("Press any key...");
                Console.ReadLine();
                return;
            }

            Console.Clear();
            Console.WriteLine("Your tasks:");
            // TODO: add paging
            taskList.ToList().ForEach(i =>
                    Console.WriteLine("Id: {0}  Name: {1} Status: {2}", i.Id, i.Name, i.Status)
            );

            Console.WriteLine("Enter Id to select a task or press any key to return...");

            int selectedId;
            if (!int.TryParse(Console.ReadLine(), out selectedId))
                return;

            Model.Task selectedTask = _taskDataAccess.GetTask(selectedId);
            if (selectedTask == null)
            {
                Console.WriteLine($"Task with Id: '{selectedId}' does not exist.");
                return;
            }

            ShowTaskOperations(selectedTask);

            Console.ReadLine();
        }

        private void ShowTaskOperations(Task task)
        {
            Console.Clear();
            Console.WriteLine("Selected Task:");
            Console.WriteLine($"{nameof(task.Id)}: {task.Id}");
            Console.WriteLine($"{nameof(task.Name)}: {task.Name}");
            Console.WriteLine($"{nameof(task.Description)}: {task.Description}");

            Console.WriteLine();
            Console.WriteLine("Choose operation or press any key to return back");
            Console.WriteLine("1. Execute");
            Console.WriteLine("2. Edit");
            Console.WriteLine("3. Delete");
            var taskAction = Console.ReadLine();

            switch (taskAction)
            {
                case "1":
                    task.Execute(_security.CurrentUser);
                    _taskDataAccess.SaveTask(task);
                    Console.WriteLine("Task was executed. Press any key to proceed...");
                    break;
                default:
                    return;
                    break;
            }
        }

        private void ShowAllTasks()
        {
            Console.Clear();
            Console.WriteLine("All tasks:");

            if (!_taskDataAccess.GetTasks().Any())
            {
                Console.WriteLine("No tasks.");
                Console.ReadKey();
                return;
            }

            int pageSize = 5;
            var count = _taskDataAccess.GetTasks().Count();
            var maxAvailablePageNo = Math.Ceiling(Decimal.Divide(count, pageSize));

            int pageNo = 1;

            do
            {
                Console.Clear();
                _taskDataAccess.GetTasks(pageNo, pageSize)
                    .ToList().ForEach(i =>
                        Console.WriteLine("Id: {0}  Name: {1} Status: {2}",
                            i.Id, i.Name, i.Status));
                Console.WriteLine();
                Console.WriteLine($"Page {pageNo} of {maxAvailablePageNo}");
                Console.WriteLine("Press any key to go to next page...");
                Console.ReadKey();
                pageNo++;
            } while (pageNo <= maxAvailablePageNo);
        }

        private void CreateTask(User user, Model.Task task)
        {
            Console.Clear();
            Console.WriteLine("New task.");
            Console.Write("Tite:");
            string name = Console.ReadLine();

            Console.Write("Description:");
            string description = Console.ReadLine();

            Model.Task newTask = new Model.Task(name, user.Id) { Description = description };
            if (task != null)
            {
                task.CopyFieldsFrom(newTask);
            }
            else
            {
                task = newTask;
            }

            _taskDataAccess.SaveTask(task);
        }

        public void ShowMenu(User user)
        {
            bool repeat = true;
            do
            {
                Console.Clear();
                Console.WriteLine("Choose action:");
                Console.WriteLine("1: Show your Tasks");
                Console.WriteLine("2: Create a new Task");
                Console.WriteLine("To quit press any key");
                string action = Console.ReadLine();

                switch (action)
                {
                    case "1":
                        ShowTasks(user);
                        break;
                    case "2":
                        CreateTask(user, null);
                        break;
                    default:
                        repeat = false;
                        break;
                }
            } while (repeat);
        }
    }
}
