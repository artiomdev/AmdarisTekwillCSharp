﻿using System;
using Tasks.SecurityAbstract;
using Tasks.Infrastructure.IoC;
using Tasks.UserInterface;

namespace Tasks
{
    class Program
    {
        private static ISecurity _security;
        private static UI _ui;

        static Program()
        {
            ServiceLocator.RegisterAll();
            _security = ServiceLocator.Get<ISecurity>();
            _ui = ServiceLocator.Get<UI>();
        }

        static void Main(string[] args)
        {
            // Приветствие
            _ui.StartMsg();

            
            // Авторизация
            _security.Authorize();
            if(_security.IsAuthorized())
                Console.WriteLine("Hello, {0}!", _security.CurrentUser.Name);
            else
                TerminateProgram();


            // Передать управление меню
            _ui.ShowMenu(_security.CurrentUser);

            // Отобразить задачи пользователя
            
            // Ожидание нажтия клавиши, для выхода из приложения
            Console.ReadKey();

            // Выход
            TerminateProgram();
        }

        /// <summary>
        /// Завершение программы
        /// </summary>
        private static void TerminateProgram()
        {
            Console.WriteLine("Good bye!");
            
            // Ожидание нажтия клавиши, для выхода из приложения
            Console.ReadKey();
            System.Environment.Exit(-1);
        }
    }
}
