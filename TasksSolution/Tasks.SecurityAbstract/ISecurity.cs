﻿using Tasks.Model;

namespace Tasks.SecurityAbstract
{
    public interface ISecurity
    {
        User CurrentUser { get; set; }

        void LogIn(string name, string password);

        void Authorize();

        bool IsAuthorized();
    }
}
