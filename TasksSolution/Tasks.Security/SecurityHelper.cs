﻿using System;
using Tasks.Model;
using Tasks.DataLayer;
using Tasks.SecurityAbstract;
using Tasks.DataLayerAbstract;

namespace Tasks.Security
{
    public class SecurityHelper: ISecurity
    {
        private IUserDataAccess _userDataAccess;


        public User CurrentUser
        {
            get; set;
        }


        public SecurityHelper(IUserDataAccess userDataAccess)
        {
            _userDataAccess = userDataAccess;
        }

        /// <summary>
        /// Запрос имени пользователя и пароля
        /// </summary>
        public void LogInUI()
        {
            Console.WriteLine("Default credentials: admin/admin");
            Console.Write("Enter your name: ");
            string name = Console.ReadLine();

            Console.Write("Enter your password: ");
            string password = Console.ReadLine();

            LogIn(name, password);
        }

        public void LogIn(string name, string password)
        {
            CurrentUser = _userDataAccess.GetUser(name, password);
        }

        /// <summary>
        /// Авторизация
        /// </summary>
        public void Authorize()
        {
            bool retry;

            do
            {
                this.LogInUI();
                retry = false;
                if (!this.IsAuthorized())
                {
                    Console.WriteLine("Authentication failed!");
                    Console.WriteLine("Would you like to try again? (y/n)");
                    retry = Console.ReadLine() == "y";
                }
            } while (retry);
        }

        /// <summary>
        /// Отвечает на вопрос: "Авторизован ли пользователь?"
        /// </summary>
        /// <returns></returns>
        public bool IsAuthorized()
        {
            return CurrentUser != null;
        }

        
    }
}