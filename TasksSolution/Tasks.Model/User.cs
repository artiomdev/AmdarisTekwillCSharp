﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Tasks.ModelAbstract;

namespace Tasks.Model
{
    public class User : Entity<int>
    {
        public override int Id { get; set; }
        public string Password { get; private set; }
        public string Name { get; private set; }
        public UserRole Role { get; private set; }

        public User(string name, string password)
        {
            if (String.IsNullOrEmpty(name))
                throw new NullReferenceException($"{nameof(User)} {nameof(name)} can't be null or empty");
            if (String.IsNullOrEmpty(password))
                throw new NullReferenceException($"{nameof(User)} {nameof(password)} can't be null or empty");

            Name = name;
            Password = password;
        }

        public override void CopyFieldsFrom(IEntity<int> userEntity)
        {
            if (userEntity == null)
                throw new NoNullAllowedException("Невозможно скопировать поля null объекта.");
            if (userEntity.Id != Id)
                throw new AccessViolationException("Неверный идентификатор. Невозможно изменить поля задачи.");
            var user = (User)userEntity;
            if ((User)user == null)
                throw new InvalidCastException($"Невозможно привести {nameof(user)} к {nameof(User)}");

            Name = user.Name;
            Role = user.Role;
            Password = user.Password;
        }

        private int CreateNextId(int Id) => ++Id;
        
        public override void CreateNextUniqueId(IEnumerable<IEntity<int>> entityCollection)
        {
            Id = CreateNextId(
                entityCollection.Any()
                    ? entityCollection.Max(i => i.Id)
                    : default(int)
                );
        }
    }
}