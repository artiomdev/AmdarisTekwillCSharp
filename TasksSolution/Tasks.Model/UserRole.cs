﻿namespace Tasks.Model
{
    public enum UserRole: short
    {
        Undefined = 0,
        Employer = 1,
        Employee = 2
    }
}