﻿using System;
using System.Collections.Generic;
using Tasks.ModelAbstract;

namespace Tasks.Model
{
    class EntityEqualityComparer<TId>: IEqualityComparer<IEntity<TId>>
    {
        public bool Equals(IEntity<TId> e1, IEntity<TId> e2)
        {
            if (e1.Id.Equals((e2.Id)))
                return true;
            return false;
        }

        public int GetHashCode(IEntity<TId> e)
        {
            return e.Id.GetHashCode();
        }
    }
}
