﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Tasks.ModelAbstract;

namespace Tasks.Model
{
    public class Task : Entity<int>
    {
        public Task(string name, int authorId)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentNullException($"{nameof(Task)} {nameof(name)} can't be null or empty");
            if (authorId == default(int))
                throw new ArgumentException($"{nameof(Task)} {nameof(authorId)} can't be zero equal");

            Name = name;
            AuthorId = authorId;
        }

        public override int Id { get; set; }

        public string Name { get; private set; }

        public int AuthorId { get; }

        public int? ExecutorId { get; private set; }

        public string Description { get; set; }

        public TaskStatus Status { get; private set; }

        /// <summary>
        /// Взять задачу в работу
        ///  - меняется статус на Active
        ///  - задаётся исполнитель
        /// </summary>
        /// <param name="user">Исполнитель</param>
        public void Execute(User user)
        {
            Status = TaskStatus.Active;
            ExecutorId = user.Id;
        }

        public override void CopyFieldsFrom(IEntity<int> taskEntity)
        {
            if (taskEntity == null)
                throw new NoNullAllowedException("Невозможно скопировать поля null объекта.");
            if (taskEntity.Id != Id)
                throw new AccessViolationException("Неверный идентификатор. Невозможно изменить поля задачи.");
            var task = (Task)taskEntity;
            if ((Task)task == null)
                throw new InvalidCastException($"Невозможно привести {nameof(task)} к {nameof(Task)}");

            Name = task.Name;
            Description = task.Description;
            ExecutorId = task.ExecutorId;
            Status = task.Status;
        }

        /// <summary>
        /// Получить уникальный идентификатор
        /// </summary>
        public override void CreateNextUniqueId(IEnumerable<IEntity<int>> entityCollection)
        {
            Id = CreateNextId(
                entityCollection.Any()
                    ? entityCollection.Max(i => i.Id)
                    : default(int)
                );
        }

        private int CreateNextId (int Id) => ++Id;
        

    }

}