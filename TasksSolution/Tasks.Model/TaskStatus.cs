﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks.Model
{
    /// <summary>
    /// Статусы задач
    /// </summary>
    public enum TaskStatus: short
    {
        /// <summary>
        /// Черновик
        /// </summary>
        Draft,

        /// <summary>
        /// Опубликована
        /// </summary>
        Published,

        /// <summary>
        /// В работе
        /// </summary>
        Active,

        /// <summary>
        /// Выполнена
        /// </summary>
        Completed,

        /// <summary>
        /// Отменена
        /// </summary>
        Cancelled
    }
}