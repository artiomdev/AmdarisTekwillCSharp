﻿using System.Configuration;
using Ninject;
using Tasks.DataLayer;
using Tasks.Storage;
using Tasks.Model;
using Tasks.Security;
using Tasks.SecurityAbstract;
using Tasks.DataLayerAbstract;
using Tasks.UserInterface;

namespace Tasks.Infrastructure.IoC
{
    public static class ServiceLocator
    {
        private static readonly IKernel _kernel = new StandardKernel();

        public static void RegisterAll()
        {
            _kernel.Bind<UI>().ToSelf();

            _kernel.Bind<ISecurity>().To<SecurityHelper>();

            _kernel.Bind<IUserDataAccess>().To<UserDataAccess>();
            _kernel.Bind<IStore<User, int>>().To<UserStorage>();

            _kernel.Bind<ITaskDataAccess>().To<TaskDataAccess>();

            _kernel.Bind<IStore<Task, int>>().To<JSONStore<Task, int>>()
                .WithConstructorArgument(
                    value: ConfigurationManager.AppSettings["TasksJSONFilePath"],
                    name: "JSONFilePath");
        }

        public static T Get<T>()
        {
            return _kernel.Get<T>();
        }
    }
}
