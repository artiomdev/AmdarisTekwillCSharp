﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Tasks.Model;
using Tasks.Storage;
using Tests.Tasks.Tests.DummyData;

namespace Tests.Tasks.Tests.StorageFixtures
{
    [TestFixture]
    public class JSONStorageTest
    {
        [SetUp]
        public void Setup()
        {
            File.WriteAllText(path: _path,
                contents: TasksTestData.TaskJsonFileContens);

            // TODO: mock source with in-mewmory content
            _store = new JSONStore<Task, int>(_path);
        }

        private JSONStore<Task, int> _store;
        private string _path = @"tasks.json";

        [Test]
        public void DeserializeTaskEntityWithExecutorIdFieldPrivateSetterReturnsNulLValue()
        {
            Assert.IsNull(_store.Entities.First().ExecutorId);
        }
    }
}
