using Moq;
using NUnit.Framework;
using Tasks.DataLayerAbstract;
using Tasks.Security;
using Tests.Tasks.Tests.DummyData;

namespace Tests.Tasks.Tests.SecurityFixtures
{
    [TestFixture]
    public abstract class SecurityHelperFixture
    {
        [SetUp]
        public void Setup()
        {
            _userDataAccess = new Mock<IUserDataAccess>();
            _userDataAccess.Setup(i => i.GetUser(TestData.TestUser.Name, TestData.TestUser.Password))
                .Returns(TestData.TestUser);

            _securityHelper = new SecurityHelper(_userDataAccess.Object);
        }

        private Mock<IUserDataAccess> _userDataAccess;
        private SecurityHelper _securityHelper;

        [TestFixture]
        public class GetAuthorizedUser : SecurityHelperFixture
        {
            [SetUp]
            public void GetAuthorizedUserSetUp () => Act();

            private void Act() => _securityHelper.LogIn(TestData.TestUser.Name, TestData.TestUser.Password);

            [Test]
            public void ItShouldSetExpectedCurrentUserProperty()
            {
                Assert.AreEqual(
                    expected: TestData.TestUser, 
                    actual: _securityHelper.CurrentUser);
            }

            [Test]
            public void ItShoulBeAuthorized()
            {
                Assert.IsTrue(condition: _securityHelper.IsAuthorized());
            }
        }
    }
}
