﻿using NUnit.Framework;
using System;
//using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tests.Tasks.Tests.DummyData;
using Tasks.Model;

namespace Tests.Tasks.Tests.DomainFixtures
{
    [TestFixture]
    public abstract class TaskTestFixture
    {
        [SetUp]
        public void Setup()
        {
            _firstTask = TasksTestData.Collection.First();
            _testUser = TestData.UserTestData.First();
        }

        private Task _firstTask;
        private User _testUser;

        [TestFixture]
        public class ExeuteTask : TaskTestFixture
        {
            [SetUp]
            public void ExecuteTaskSetUp () => _firstTask.Execute(_testUser);

            [Test]
            public void ItsExecutionShouldSetExecutor () => 
                Assert.IsNotNull(anObject: _firstTask.ExecutorId);
            
            [Test]
            public void ItsExecutionShouldSetUserAsExecutor()
            {
                Assert.AreEqual(
                    expected: _testUser.Id,
                    actual: _firstTask.ExecutorId);
            }

            [Test]
            public void ItsExecutionShouldSetActiveStatus()
            {
                Assert.AreEqual(
                    expected: _firstTask.Status,
                    actual: TaskStatus.Active);
            }
        }

        [TestFixture]
        public class CopyFieldsFrom : TaskTestFixture
        {
            [SetUp]
            public void CopyFieldsFromSetUp() { }

            [Test]
            [TestCase(null, 0)]
            [TestCase("", 1)]
            public void ItShouldThowNullArgumentException(string name, int authorId) 
                => Assert.Throws<ArgumentNullException>(
                    () => new Task(name, authorId));
        }
    }
}
