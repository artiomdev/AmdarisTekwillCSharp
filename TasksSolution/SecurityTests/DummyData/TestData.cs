﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tasks.Model;

namespace Tests.Tasks.Tests.DummyData
{
    internal static class TestData
    {
        internal static IList<User> UserTestData => new List<User>()
        {
            new User("admin", "admin") {Id = 1},
            new User("Artiom", "pass") {Id = 2},
            new User("Alex", "pass") {Id = 3}
        };

        internal static User TestUser { get; } = UserTestData.FirstOrDefault();

    }
}
