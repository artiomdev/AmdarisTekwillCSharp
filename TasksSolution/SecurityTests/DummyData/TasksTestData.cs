﻿using System.Collections.Generic;
using Tasks.Model;

namespace Tests.Tasks.Tests.DummyData
{
    internal static class TasksTestData
    {
        internal static IList<Task> Collection => new List<Task>()
        {
            new Task(name: "First Task",  authorId: 1) { Id = 1 },
            new Task(name: "second Task", authorId: 1) { Id = 2 },
            new Task(name: "Third Task",  authorId: 2) { Id = 1 }
        };

        internal static string TaskJsonFileContens => @"
                        [{
	                        'Id': 1,

                            'Name': '1',
	                        'AuthorId': 1,
	                        'ExecutorId': 1,
	                        'Description': '2',
	                        'Status': 2
                        },
                        {
                                        'Id': 2,
	                        'Name': 'test',
	                        'AuthorId': 1,
	                        'ExecutorId': null,
	                        'Description': 'test',
	                        'Status': 0
                        },
                        {
                                        'Id': 3,
	                        'Name': 'test2',
	                        'AuthorId': 1,
	                        'ExecutorId': null,
	                        'Description': 'test2',
	                        'Status': 0
                        },
                        {
                                        'Id': 4,
	                        'Name': 'test3',
	                        'AuthorId': 1,
	                        'ExecutorId': null,
	                        'Description': 'test3',
	                        'Status': 0
                        },
                        {
                                        'Id': 5,
	                        'Name': 'My task',
	                        'AuthorId': 1,
	                        'ExecutorId': null,
	                        'Description': '',
	                        'Status': 0
                        }]
                        ";
    }
}
