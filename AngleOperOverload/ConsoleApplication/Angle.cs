﻿using System;
using System.Diagnostics;

namespace ConsoleApplication
{
    public struct Angle
    {
        private const ushort SEXSAGESIMAL = 60;
        private const ushort DEGREE_IN_A_TURN = 360;

        public int Degree;
        public int Minutes;
        public int Seconds;

        public Angle (int degree, int minutes, int seconds)
        {
            if (seconds >= SEXSAGESIMAL) throw new ArithmeticException();
            if (minutes >= SEXSAGESIMAL) throw new ArithmeticException();

            Degree = degree;
            Minutes = minutes;
            Seconds = seconds;
        }
        public Angle (Angle angle)
        {
            Degree = angle.Degree;
            Minutes = angle.Minutes;
            Seconds = angle.Seconds;
        }
        public Angle (int rawSeconds)
        {
            int turns = rawSeconds / SEXSAGESIMAL / SEXSAGESIMAL / DEGREE_IN_A_TURN;

            Degree = rawSeconds / SEXSAGESIMAL / SEXSAGESIMAL % DEGREE_IN_A_TURN;
            Minutes = Math.Abs(rawSeconds / SEXSAGESIMAL % SEXSAGESIMAL);
            Seconds = Math.Abs(rawSeconds % SEXSAGESIMAL);
        }

        public override string ToString()
        {
            return String.Format("{0}{3}{1}\'{2}\"", Degree, Minutes, Seconds, (char)176);
        }
        public int ToSeconds()
        {
            return this.Seconds + (this.Minutes * SEXSAGESIMAL) + (this.Degree * SEXSAGESIMAL * SEXSAGESIMAL);
        }

        #region overloading arithmetic operations
        #region ВАРИАНТ РЕШЕНИЯ №1
        /**
         * ВАРИАНТ РЕШЕНИЯ №1 на примере операции сложения
         * Этот метод реализован в соответствии с первой идеей решения задачи
         *  Я специально не переделал его на более к4расивый, чтоб показать ход мыслей
         */
        public static Angle operator +(Angle lhs, Angle rhs)
        {
            // вычисляем секунды, исключая сформировавшиеся минуты
            int seconds = (lhs.Seconds + rhs.Seconds) % SEXSAGESIMAL;
            // вычисляем минуты, сформировавшиеся из секунд
            int extraMinutes = (((lhs.Seconds + rhs.Seconds) - seconds) / SEXSAGESIMAL);
            // при сложении секунд двух правильно заданных углов,
            //  количество сформировавшихся из скунд минут не может превышать 1
            Debug.Assert(extraMinutes <= 1);

            int minutes = (lhs.Minutes + rhs.Minutes + extraMinutes) % SEXSAGESIMAL;
            int extraDegree = (lhs.Minutes + rhs.Minutes + extraMinutes - minutes) / SEXSAGESIMAL;
            Debug.Assert(extraDegree <= 1);

            int degree = (lhs.Degree + rhs.Degree + extraDegree) % DEGREE_IN_A_TURN;
            // если сумма градусов больше либо равна 360, начисляются обороты
            int turns = (lhs.Degree + rhs.Degree + extraDegree - degree) / DEGREE_IN_A_TURN;
            Debug.Assert(turns <= 1);

            return new Angle(degree, minutes, seconds);
        }
        #endregion ВАРИАНТ РЕШЕНИЯ №1

        /**
        * ВАРИАНТ РЕШЕНИЯ №2 "красивый"
        * Этот метод написан красиво, но догадался не сразу
        *  поэтому выше описан первый вариан тешения задачи
        */
        public static Angle operator -(Angle lhs, Angle rhs)
        {
            int resultSeconds = lhs.ToSeconds() - rhs.ToSeconds();

            return new Angle(resultSeconds);
        }
        public static Angle operator *(Angle lhs, int rhs)
        {
            int resultSeconds = lhs.ToSeconds() * rhs;

            return new Angle(resultSeconds);
        }
        public static Angle operator *(int lhs, Angle rhs)
        {
            int resultSeconds = lhs * rhs.ToSeconds();

            return new Angle(resultSeconds);
        }
        public static Angle operator /(Angle lhs, int rhs)
        {
            int resultSeconds = lhs.ToSeconds() / rhs;

            return new Angle(resultSeconds);
        }
        #endregion overloading arithmetic operations

        #region overloading logical operations
        public static bool operator <(Angle lhs, Angle rhs)
        {
            return lhs.ToSeconds() < rhs.ToSeconds();
        }
        public static bool operator >(Angle lhs, Angle rhs)
        {
            return lhs.ToSeconds() > rhs.ToSeconds();
        }
        public static bool operator ==(Angle lhs, Angle rhs)
        {
            return lhs.ToSeconds() == rhs.ToSeconds();
        }
        public static bool operator !=(Angle lhs, Angle rhs)
        {
            return lhs.ToSeconds() != rhs.ToSeconds();
        }
        public static bool operator <=(Angle lhs, Angle rhs)
        {
            return lhs.ToSeconds() <= rhs.ToSeconds();
        }
        public static bool operator >=(Angle lhs, Angle rhs)
        {
            return lhs.ToSeconds() >= rhs.ToSeconds();
        }
        #endregion overloading logical operations
    }
}