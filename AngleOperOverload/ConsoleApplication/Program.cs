﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var a1 = new Angle(3, 30, 20);
            var a2 = new Angle(1, 31, 21);

            Console.WriteLine("{0} + {1} = {2}", a1, a2, a1 + a2);

            Console.WriteLine("{0} - {1} = {2}", a1, a2, a1 - a2);
            Console.WriteLine("{0} - {1} = {2}", a2, a1, a2 - a1);

            Console.WriteLine("{0} * {1} = {2}", a2, 5, a2 * 5);
            Console.WriteLine("{0} * {1} = {2}", 5, a1, 5 * a1);

            Console.WriteLine("{0} / {1} = {2}", a2, 4, a2 / 4);


            Console.WriteLine("{0} == {1}: {2}", a1, a2, a1 == a2);
            Console.WriteLine("{0} != {1}: {2}", a1, a2, a1 != a2);

            Console.WriteLine("{0} > {1}: {2}", a1, a2, a1 > a2);
            Console.WriteLine("{0} < {1}: {2}", a1, a2, a1 < a2);

            Console.WriteLine("{0} >= {1}: {2}", a1, a2, a1 >= a2);
            Console.WriteLine("{0} <= {1}: {2}", a1, a2, a1 <= a2);

            Console.ReadKey();
        }
    }
}